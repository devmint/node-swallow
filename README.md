<div align="center">
    <img src="./logo.png" alt="node-swallow" />
</div>

<div align="center">
    <strong>Different approach to catching errors in Javascript (alternative to try/catch clause).</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%232b7489.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/devmint/node-swallow/master?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/coverage/devmint/node-swallow/master?style=for-the-badge" />
</div>

<br />
<br />

## Installation

First you need to setup registry:

```bash
echo @devmint:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
```

or

```bash
echo \"@devmint:registry\" \"https://gitlab.com/api/v4/packages/npm/\" >> .yarnrc
```

Then you can install package using `npm`

```bash
npm i @devmint/node-swallow
```

or `yarn`

```bash
yarn add @devmint/node-swallow
```


## Usage

Purpose of this library is to write multiple catch-clauses in nice-way without writing many `if/else`
statements. To show example, lets say our application layer should throw a HTTP error from controller
but our domain layer returns domain's errors.

```tsx
async function showProduct(productId: number): string {
  try {
    return await findProduct(productId)
  } catch (e) {
    if (e instanceof ProductNotFoundError) {
      throw new HttpNotFoundError(e.message)
    }

    console.error('product not found', e)
    throw e
  }
}
```

We can rewrite this example to something like this:

```tsx
import {swallowSync, catchIf, catchLog} from '@devmint/node-swallow'

async function showProduct(productId: number): string {
  try {
    return await findProduct(productId)
  } catch (e) {
    swallowSync(
      catchLog('product not found'),
      catchIf(ProductNotFoundError, (e) => new HttpNotFoundError(e.message))
    )(e)
  }
}
```

## Asynchronous or synchronous execution

Package exports two different swallow functions: `swallow` and `swallowSync`. Most of time
you should use `swallowSync` because catchers are synchronous. Exception of this rule is `catchIfReturn`
which can return `Promise`.

```tsx
import {swallow, catchIfReturn, catchAlways} from '@devmint/node-swallow'

async function loremIpsum() {
  try {
    return await findDolor()
  } catch (e) {
    return await swallow(
      catchAlways(() => new DolorNotFound()),
      catchIfReturn(DolorNotFound, () => createDolor())
    )(e)
  }
}

function createDolor(): Promise<Dolor> {
  return createNewDolor({ dolor: 'ipsum' })
}
```

Example shows that function `loremIpsum()` should always return `Promise<Dolor>` even when function
`findDolor()` cannot find dolor in repository. When `findDolor()` throw some unknown error (e.g. cannot
connect to the database) then we still throw an error.

It's worth to use this in `findOrCreate()` functions where we want to create (and sometimes persist) item
which does not exists.

## Catchers

Swallow requires _catchers_ to execute some actions. Common definition for _catcher_ is:

```typescript
/**
 * Definition for custom "catchers". Catchers are some kind factories which should
 * return ReturnHandler<R, T>.
 *
 * @param e Instance of an error. It's possible that error does not extends Error class
 *          but is plain string, number or different type.
 * @param opts Options applied to catcher, optional.
 */
export type ReturnHandler<R = Error, T = Error | string> = (
  e: T,
  opts?: HandleErrorOptions,
) => R | undefined
```

Package `node-swallow` comes with some prebuilt catchers:

* `catchAlways`: Transforms one type of error to another without doing any matching. It's nice to put this catcher as last in the order.
* `catchIf`: Transform one type of error to another when type of error matches _"errType"_.
* `catchIfMatch`: Transform one type of error to another when type of error matches _"errType"_. Matching is done by comparing errors' content (`err.message`) or when error is plain string, compare strings.
* `catchIfReturn`: Compares _"errType"_ to caught error and execute callback. Returns value from callback when error match, otherwise throws received error.
* `catchLog`: Writes message to passed logger (or uses internal `console`) with received message. Also attach to _"meta"_ object additional field named _"debug_message"_ with error's message or predefined string about that error is not a valid error.