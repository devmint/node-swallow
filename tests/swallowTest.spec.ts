import { swallow, catchIfReturn, catchIf, catchIfMatch, catchAlways } from '../src'

class FirstError extends Error {}
class SecondError extends Error {}
class ThirdError extends Error {}

describe('swallow', () => {
  describe('with async/await', () => {
    const plainError = new Error('it is not second error!')

    it('should allow to use only one catcher', async () => {
      try {
        await swallow(catchIf(SecondError, () => new ThirdError()))(plainError)
      } catch (e) {
        expect(e).toBeInstanceOf(Error)
        expect((e as Error).message).toEqual('it is not second error!')
      }
    })

    it('should run catchers from left to right', async () => {
      const result = await swallow(
        catchIf(FirstError, () => new SecondError('second error')),
        catchIfReturn(SecondError, () => 2),
      )(new FirstError('first error'))

      expect(result).toEqual(2)
    })

    it('should handle all catchers when nothing should return', async () => {
      try {
        await swallow(
          catchIf(FirstError, () => new SecondError()),
          catchIf(SecondError, () => new ThirdError()),
        )(new FirstError())
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
      }
    })

    it('should handle all catchers and resolve promises', async () => {
      const randomCallback = () => Promise.reject(new SecondError())

      const result = await swallow(
        catchIfReturn(FirstError, randomCallback),
        catchIfReturn(SecondError, () => 12),
      )(new FirstError())
      expect(result).toEqual(12)
    })

    it('should handle async error handler', async () => {
      const result = await swallow(
        catchIf(FirstError, () => new SecondError('second error')),
        catchIfReturn(SecondError, () => Promise.resolve(2)),
      )(new FirstError('first error'))

      expect(result).toEqual(2)
    })

    it('should handle string error', async () => {
      try {
        await swallow(
          catchIf(FirstError, () => new SecondError('it is not string')),
          catchIfMatch('error string', () => new SecondError('it is string')),
        )('random error string')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        expect((e as SecondError).message).toEqual('it is string')
      }
    })

    it('should stop rest of the catchers when the first one returns value', async () => {
      const result = await swallow(
        catchIfReturn(FirstError, () => 2),
        catchIf(FirstError, () => new ThirdError('third error')),
      )(new FirstError('first error'))

      expect(result).toEqual(2)
    })

    it('should throw original error when any matcher does not exists', async () => {
      try {
        await swallow(
          catchIf(SecondError, () => new ThirdError()),
          catchIf(ThirdError, () => new ThirdError()),
        )(new FirstError('first error'))
      } catch (e) {
        expect(e).toBeInstanceOf(FirstError)
      }
    })

    it('should throw original error from async callback', async () => {
      const wrap = async () => {
        try {
          throw new ThirdError('lorem ipsum')
        } catch (e) {
          return swallow(catchIf(FirstError, () => new SecondError()))(e)
        }
      }

      try {
        await wrap()
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
      }
    })
  })

  describe('weird scenarios (null, undefined)', () => {
    it('should return undefined when catchIfReturn wants to return undefined', async () => {
      const result = await swallow(
        catchIf(FirstError, () => new SecondError('should throw an error')),
        catchIfReturn(SecondError, () => undefined),
      )(new FirstError())

      expect(result).toBeUndefined()
    })

    it('should return null when catchIfReturn wants to return null', async () => {
      const result = await swallow(
        catchIf(FirstError, () => new SecondError('should throw an error')),
        catchIfReturn(SecondError, () => null),
      )(new FirstError())

      expect(result).toBeNull()
    })
  })

  it('should throw unexpected error when any matchers match', async () => {
    try {
      await swallow(
        catchIf(FirstError, () => new SecondError('should not throw')),
        catchAlways(() => new ThirdError('should throw')),
      )(new SecondError('matchers do not match'))
    } catch (e) {
      expect(e).toBeInstanceOf(ThirdError)
    }
  })
})
