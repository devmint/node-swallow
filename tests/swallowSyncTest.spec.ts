import { swallowSync, catchIfReturn, catchIf, catchIfMatch, catchAlways } from '../src'

class FirstError extends Error {}
class SecondError extends Error {}
class ThirdError extends Error {}

describe('swallowSync', () => {
  it('should allow to use only one catcher', (done) => {
    try {
      swallowSync(catchIf(SecondError, () => new ThirdError()))(
        new Error('it is not second error!'),
      )
      done.fail('should throw an error!')
    } catch (e) {
      expect(e).toBeInstanceOf(Error)
      expect((e as Error).message).toEqual('it is not second error!')
      done()
    }
  })

  it('should run catchers from left to right', () => {
    const result = swallowSync(
      catchIf(FirstError, () => new SecondError('second error')),
      catchIfReturn(SecondError, () => 2),
    )(new FirstError('first error'))

    expect(result).toEqual(2)
  })

  it('should handle all catchers when nothing should return', () => {
    try {
      swallowSync(
        catchIf(FirstError, () => new SecondError()),
        catchIf(SecondError, () => new ThirdError()),
      )(new FirstError())
    } catch (e) {
      expect(e).toBeInstanceOf(ThirdError)
    }
  })

  it('should handle async error handler', (done) => {
    const result = swallowSync(
      catchIf(FirstError, () => new SecondError('second error')),
      catchIfReturn(SecondError, () => Promise.resolve(2)),
    )(new FirstError('first error'))

    if (result instanceof Promise) {
      result.then((r) => {
        expect(r).toEqual(2)
        done()
      })
    } else {
      done.fail('Variable "result" should not be empty!')
    }
  })

  it('should handle string error', () => {
    try {
      swallowSync(
        catchIf(FirstError, () => new SecondError('it is not string')),
        catchIfMatch('error string', () => new SecondError('it is string')),
      )('random error string')
    } catch (e) {
      expect(e).toBeInstanceOf(SecondError)
      expect((e as SecondError).message).toEqual('it is string')
    }
  })

  it('should stop rest of the catchers when the first one returns value', () => {
    const result = swallowSync(
      catchIfReturn(FirstError, () => 2),
      catchIf(FirstError, () => new ThirdError('third error')),
    )(new FirstError('first error'))

    expect(result).toEqual(2)
  })

  it('should throw original error when any matcher does not exists', () => {
    try {
      swallowSync(
        catchIfReturn(SecondError, () => 1),
        catchIfReturn(ThirdError, () => 2),
      )(new FirstError('first error'))
    } catch (e) {
      expect(e).toBeInstanceOf(FirstError)
    }
  })

  describe('weird scenarios (null, undefined)', () => {
    it('should return undefined when catchIfReturn wants to return undefined', () => {
      const result = swallowSync(
        catchIf(FirstError, () => new SecondError('should throw an error')),
        catchIfReturn(SecondError, () => undefined),
      )(new FirstError())

      expect(result).toBeUndefined()
    })

    it('should return null when catchIfReturn wants to return null', () => {
      const result = swallowSync(
        catchIf(FirstError, () => new SecondError('should throw an error')),
        catchIfReturn(SecondError, () => null),
      )(new FirstError())

      expect(result).toBeNull()
    })
  })

  it('should throw unexpected error when any matchers match', () => {
    try {
      swallowSync(
        catchIf(FirstError, () => new SecondError('should not throw')),
        catchAlways(() => new ThirdError('should throw')),
      )(new SecondError('matchers do not match'))
    } catch (e) {
      expect(e).toBeInstanceOf(ThirdError)
    }
  })
})
