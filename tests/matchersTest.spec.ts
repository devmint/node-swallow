import { catchIfReturn, catchIf, catchIfMatch, catchLog, catchAlways, SwallowError } from '../src'

class FirstError extends Error {}
class SecondError extends Error {}
class ThirdError extends Error {}

describe('matchers', () => {
  describe('catchIf', () => {
    it('should throw an error from callback when argument match', (done) => {
      try {
        catchIf(FirstError, () => new SecondError())(new FirstError())
        done.fail('should throw an error')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should allow to match multiple error types with one callback', (done) => {
      try {
        catchIf([FirstError, SecondError], () => new ThirdError())(new SecondError())
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
        done()
      }
    })

    it('should return undefined when array of matchers is empty', () => {
      const result = catchIf([], () => new ThirdError())(new SecondError())
      expect(result).toBeUndefined()
    })

    it('should return undefined when argument does not match', () => {
      const result = catchIf(FirstError, () => new SecondError())(new SecondError())
      expect(result).toBeUndefined()
    })

    it('should return undefined when error is just string', () => {
      const result = catchIf(FirstError, () => new SecondError())("it's just string")
      expect(result).toBeUndefined()
    })

    it('should throw an error when I use option `throwOnFail`', (done) => {
      try {
        catchIf(FirstError, () => new SecondError('second error'))(new ThirdError(), {
          throwOnFail: true,
        })
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
        done()
      }
    })
  })

  describe('catchIfReturn', () => {
    const error = new SecondError('catchIfReturn error')

    it('should return value from callback when argument match', () => {
      const result = catchIfReturn(SecondError, () => 12)(error)
      expect(result).toEqual(12)
    })

    it('should allow to match multiple error types with one callback', () => {
      const result = catchIfReturn([FirstError, SecondError], () => 12)(error)
      expect(result).toEqual(12)
    })

    it('should return undefined when argument does not match', () => {
      const result = catchIfReturn(FirstError, () => 12)(error)
      expect(result).toBeUndefined()
    })

    it('should return undefined when error is just string', () => {
      const result = catchIfReturn(FirstError, () => 12)(error.message)
      expect(result).toBeUndefined()
    })

    it('should throw an error when I use option `throwOnFail`', (done) => {
      try {
        catchIfReturn(FirstError, () => new SecondError('second error'))(new ThirdError(), {
          throwOnFail: true,
        })
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
        done()
      }
    })
  })

  describe('catchIfMatch', () => {
    const actual = new FirstError('this is exception')
    const expected = new SecondError()

    it('should throw an error from callback when argument match (plain string)', (done) => {
      try {
        catchIfMatch('this is', () => expected)(actual)
        done.fail('should throw an error')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should throw an error from callback when argument match (error is a string)', (done) => {
      try {
        catchIfMatch('this is', () => expected)(actual.message)
        done.fail('should throw an error')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should throw an error from callback when argument match (regexp)', (done) => {
      try {
        catchIfMatch(/User (\d+) not found/, () => expected)(new FirstError('User 12 not found'))
        done.fail('should throw an error')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should allow to match multiple error types with one callback', (done) => {
      try {
        catchIfMatch(
          ['Lorem ipsum', /dolor sit amet/, /User (\d+) not found/],
          () => expected,
        )(new Error('User 12 not found in the database'))
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should return undefined when argument does not match', () => {
      const result = catchIfMatch('thisnot', () => expected)(actual)
      expect(result).toBeUndefined()
    })

    it('should return undefined when error is just string', () => {
      const result = catchIfMatch('thisnot', () => expected)(actual.message)
      expect(result).toBeUndefined()
    })

    it('should throw an error when I use option `throwOnFail`', (done) => {
      try {
        catchIfMatch('first', () => new SecondError('second error'))(
          new ThirdError('third error'),
          {
            throwOnFail: true,
          },
        )
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(ThirdError)
        done()
      }
    })
  })

  describe('catchLog', () => {
    const error = new FirstError('first error it is!')
    const logger = {
      info: jest.fn(),
      warn: jest.fn(),
      error: jest.fn(),
    }

    beforeEach(() => {
      jest.resetAllMocks()
    })

    it('should just print log to logger', () => {
      catchLog('log message from test', {
        test: 'should just print log to logger',
      })(error, { logger })
      expect(logger.error).toHaveBeenCalled()
    })

    for (const logLevel of ['info', 'warn', 'error']) {
      it(`should allow to change log level to ${logLevel}`, () => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        catchLog(`log message for ${logLevel}`, {}, <any>logLevel)(error, { logger })
      })
    }

    it('should throw a SwallowError when logger level is invalid', (done) => {
      try {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        catchLog('log message', {}, <any>'lorem')(error, { logger })
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(SwallowError)
        expect((e as SwallowError).message).toContain('catchLog')
        done()
      }
    })

    it('should return undefined', () => {
      const result = catchLog('should return undefined, always')(error, { logger })
      expect(result).toBeUndefined()
    })
  })

  describe('catchAlways', () => {
    const error = new FirstError('first error it is!')

    it('should catch error and throw another one', (done) => {
      try {
        catchAlways(() => new SecondError('second error it is!'))(error)
        done.fail('should throw error')
      } catch (e) {
        expect(e).toBeInstanceOf(SecondError)
        done()
      }
    })

    it('should catch "error" when error is undefined', (done) => {
      try {
        // @ts-ignore: undefined is not a valid Error type
        catchAlways(() => error)(undefined)
        done.fail('should throw an error!')
      } catch (e) {
        expect(e).toBeInstanceOf(FirstError)
        done()
      }
    })
  })
})
