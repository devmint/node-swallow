import { SwallowError, UnexpectedErrorCaught } from '../errors'
import { isError, isString, ReturnHandler, ThrowCallback } from './definitions'

/**
 * Transforms one type of error to another without doing any matching. It's nice to put
 * this catcher as last in the order.
 *
 * Example:
 *   catchAlways(() => new InternalServerError())(e)
 *
 * @param cb Callback executed when error is valid error (or string)
 */
export function catchAlways<R = Error>(cb: ThrowCallback<Error, R>): ReturnHandler<undefined> {
  return function (e: unknown): undefined {
    if (isError(e)) {
      throw cb(e)
    }
    if (isString(e)) {
      throw cb(new SwallowError(e))
    }
    throw cb(new UnexpectedErrorCaught('catchAlways'))
  }
}
