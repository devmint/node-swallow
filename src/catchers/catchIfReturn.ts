import {
  ErrorMessage,
  HandleErrorOptions,
  isError,
  ReturnHandler,
  ThrowCallback,
} from './definitions'

/**
 * Compares "errType" to caught error and execute callback. Returns
 * value from callback when error match, otherwise throws received
 * error.
 *
 * Example:
 *   const product = catchIfReturn(ProductNotFoundError, () => {
 *     return new Product();
 *   })(e)
 *
 * @param errType What kind of error you want to catch.
 * @param cb Callback executed when "errType" match to type of error.
 */
export function catchIfReturn<R, T = Error>(
  errType: ErrorMessage<T> | Array<ErrorMessage<T>>,
  cb: ThrowCallback<T, R>,
): ReturnHandler<R> {
  return function (e: unknown, opts?: HandleErrorOptions): R | undefined {
    const errorInstances = Array.isArray(errType) ? errType : [errType]

    if (isError(e)) {
      for (const singleErrType of errorInstances) {
        if (e instanceof singleErrType) {
          return cb(e)
        }
      }
    }

    if (opts?.throwOnFail) {
      throw e
    }
    return
  }
}
