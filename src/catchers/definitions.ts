import * as utils from 'util'

export interface Logger {
  info(msg: string, ...args: unknown[]): void
  warn(msg: string, ...args: unknown[]): void
  error(msg: string, ...args: unknown[]): void
}

export interface HandleErrorOptions {
  /**
   * Swallow by default (and some catchers) do not rethrow passed error. To force
   * rethrow we need to tell explicitly that we want to rethrow the error.
   */
  throwOnFail?: boolean

  /**
   * Custom instance of logger used for `catchLog`. It allows us to overwrite logger
   * from 'catchLogFactory' in some kind of situations.
   */
  logger?: Logger
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ErrorMessage<T = Error> = { new (...args: any[]): T }
export type ThrowCallback<T = Error, R = Error> = (e: T) => R

/**
 * Definition for custom "catchers". Catchers are some kind factories which should
 * return ReturnHandler<R, T>.
 *
 * @param e Instance of an error. It's possible that error does not extends Error class
 *          but is plain string, number or different type.
 * @param opts Options applied to catcher, optional.
 */
export type ReturnHandler<R = Error, T = Error | string> = (
  e: T,
  opts?: HandleErrorOptions,
) => R | undefined

export function isError(e: unknown): e is Error {
  return utils.types.isNativeError(e)
}

export function isString(e: unknown): e is string {
  return typeof e === 'string' && e !== ''
}
