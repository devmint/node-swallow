import {
  ErrorMessage,
  HandleErrorOptions,
  isError,
  ReturnHandler,
  ThrowCallback,
} from './definitions'

/**
 * Transform one type of error to another when type of error matches "errType".
 *
 * Example:
 *   catchIf(ProductNotFoundError, () => HttpNotFoundException())(e)
 *
 * @param errType What kind of error you want to catch.
 * @param cb Callback executed when "errType" match to type of error.
 */
export function catchIf<R, T = Error>(
  errType: ErrorMessage<T> | Array<ErrorMessage<T>>,
  cb: ThrowCallback<T, R>,
): ReturnHandler<undefined> {
  return function (e: unknown, opts?: HandleErrorOptions): undefined {
    const errorInstances = Array.isArray(errType) ? errType : [errType]

    if (isError(e)) {
      for (const singleErrType of errorInstances) {
        if (e instanceof singleErrType) {
          throw cb(e)
        }
      }
    }

    if (opts?.throwOnFail) {
      throw e
    }
    return
  }
}
