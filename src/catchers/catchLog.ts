import { SwallowError } from '../errors'
import { HandleErrorOptions, isError, Logger, ReturnHandler } from './definitions'

const defaultConsoleLogger: Logger = {
  info: (msg: string, ...args: unknown[]) => console.info(msg, ...args),
  warn: (msg: string, ...args: unknown[]) => console.warn(msg, ...args),
  error: (msg: string, ...args: unknown[]) => console.error(msg, ...args),
}

/**
 * Creates an instance of `catchLog` function with custom logger.
 *
 * @param logger Custom instance of logger
 */
export function catchLogFactory(logger: Logger = defaultConsoleLogger) {
  return function catchLog(
    msg: string,
    meta?: Record<string, unknown>,
    level: keyof Logger = 'error',
  ): ReturnHandler<undefined> {
    return function (e: unknown, opts?: HandleErrorOptions): undefined {
      if (false === level in console) {
        throw new SwallowError('Invalid level of logger in catchLog is used!')
      }

      const l = opts?.logger || logger
      l[level](msg, {
        ...meta,
        debug_message: isError(e) ? e.message : 'Argument "e" is not a native error',
      })

      if (opts?.throwOnFail) {
        throw e
      }

      return
    }
  }
}

/**
 * Writes message to passed logger (or uses internal `console`) with received message.
 * Also attach to "meta" object additional field named "debug_message" with error's message
 * or predefined string about that error is not a valid error.
 *
 * Example:
 *   catchLog('Probably product not found', { product_id: 123 })(e)
 * Output (from `console.error`):
 *   'Probably product not found' { product_id: 123, debug_message: 'ID 123 not found' }
 *
 * @param msg Message which you want to log
 * @param meta Arguments passed to message e.g. some objects or meta, optional
 * @param level Level of used logger, allowed values are "info", "warn" and "error", optional
 */
export const catchLog = catchLogFactory()
