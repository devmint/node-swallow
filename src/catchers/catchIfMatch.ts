import { SwallowError } from '../errors'
import { HandleErrorOptions, isError, isString, ReturnHandler, ThrowCallback } from './definitions'

/**
 * Transform one type of error to another when type of error matches "errType". Matching
 * is done by comparing errors' content (err.message) or when error is plain string, compare
 * strings.
 *
 * Example:
 *   catchIfMatch('product not found', () => HttpNotFoundException())(e)
 *   catchIfMatch(/not found$/, () => HttpNotFoundException())(e)
 *
 * @param errType What kind of error you want to catch. Differently from built-in catchers, this one
 *                does not check type of error but compares its message.
 * @param cb Callback executed when "errType" match to type of error.
 */
export function catchIfMatch<R>(
  errType: string | RegExp | Array<string | RegExp>,
  cb: ThrowCallback<Error, R>,
): ReturnHandler<undefined> {
  return function (e: unknown, opts?: HandleErrorOptions): undefined {
    const errorInstances = Array.isArray(errType) ? errType : [errType]

    for (const singleErrType of errorInstances) {
      if (isError(e) && e.message.match(singleErrType)) {
        throw cb(e)
      }
      if (isString(e) && e.match(singleErrType)) {
        throw cb(new SwallowError(e))
      }
    }

    if (opts?.throwOnFail) {
      throw e
    }
    return
  }
}
