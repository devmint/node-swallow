import { HandleErrorOptions, isError, isString, ReturnHandler } from './catchers/definitions'

type HandleError<R> = (e: unknown, opts?: HandleErrorOptions) => R

/**
 * Similar to `swallow` but with one difference - clauses passes as arguments
 * cannot by asynchronous functions. If you know that all your "catchers" are
 * synchronous it's better to use `swallowSync`.
 */
export function swallowSync<R1>(cl1: ReturnHandler<R1>): HandleError<R1 | void>
export function swallowSync<R1, R2>(
  cl1: ReturnHandler<R1>,
  cl2: ReturnHandler<R2>,
): HandleError<R1 | R2 | void>
export function swallowSync<R1, R2, R3>(
  cl1: ReturnHandler<R1>,
  cl2: ReturnHandler<R2>,
  cl3: ReturnHandler<R3>,
): HandleError<R1 | R2 | R3 | void>
export function swallowSync<R1, R2, R3, R4>(
  cl1: ReturnHandler<R1>,
  cl2: ReturnHandler<R2>,
  cl3: ReturnHandler<R3>,
  cl4: ReturnHandler<R4>,
): HandleError<R1 | R2 | R3 | R4 | void>
export function swallowSync<R1, R2, R3, R4, R5>(
  cl1: ReturnHandler<R1>,
  cl2: ReturnHandler<R2>,
  cl3: ReturnHandler<R3>,
  cl4: ReturnHandler<R4>,
  cl5: ReturnHandler<R5>,
): HandleError<R1 | R2 | R3 | R4 | R5 | void>

export function swallowSync<R1, R2, R3, R4, R5>(
  cl1: ReturnHandler<R1>,
  cl2?: ReturnHandler<R2>,
  cl3?: ReturnHandler<R3>,
  cl4?: ReturnHandler<R4>,
  cl5?: ReturnHandler<R5>,
): HandleError<R1 | R2 | R3 | R4 | R5 | void> {
  const clauses = [cl1, cl2, cl3, cl4, cl5]

  return function (e: unknown, opts?: HandleErrorOptions): R1 | R2 | R3 | R4 | R5 | void {
    if (typeof e === 'undefined') {
      throw e
    }
    if (!isString(e) && !isError(e)) {
      throw e
    }

    const swallowOptions: HandleErrorOptions = {
      ...opts,
      throwOnFail: true,
    }

    let result = e
    for (const clause of clauses) {
      if (!clause) {
        continue
      }

      try {
        const data = clause(result, swallowOptions)
        if (!isError(data)) {
          return data
        }
      } catch (caught) {
        if (typeof caught === 'string' || caught instanceof Error) {
          result = caught
        }
      }
    }

    throw result
  }
}
