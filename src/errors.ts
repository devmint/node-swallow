export class SwallowError extends Error {}

export class UnexpectedErrorCaught extends SwallowError {
  constructor(methodName = 'catch*/swallow') {
    super(`Unexpected error caught (not an error or string) during ${methodName} method!`)
  }
}
